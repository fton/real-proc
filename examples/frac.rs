use real_proc::frac;
use const_frac::Frac;

const CONST: Frac = frac!(const_frac, 3.14e-20);

fn main() {
    let int = Frac::from_int(314);
    let right = int * Frac::from_exp10(-22);

    assert_eq!(CONST, right);
}