use real_proc::frac;

fn main() {
    let int = frac!(const_frac, 1e20);

    assert_eq!(int.to_f64(), 1e20);
}