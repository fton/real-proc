[![Latest Release](https://gitlab.com/fton/real-proc/-/badges/release.svg)](https://gitlab.com/fton/real-proc/-/releases)
[![pipeline status](https://gitlab.com/fton/real-proc/badges/main/pipeline.svg)](https://gitlab.com/fton/real-proc/-/commits/main)
[![coverage report](https://gitlab.com/fton/real-proc/badges/main/coverage.svg)](https://gitlab.com/fton/real-proc/-/commits/main)
# Description
Proc-macro for generating [Frac] from a decimal literal in const context.
# Usage
```
use ::real_proc::frac;
use ::const_frac::Frac;

const CONST: Frac = frac!(const_frac, 3.14e-20);

let int = Frac::from_int(314);
let right = int * Frac::from_exp10(-22);

assert_eq!(CONST, right);
```
# Project status
This package is in the very early stage.

[Frac]: https://docs.rs/const-frac/*/const_frac/frac/struct.Frac.html