#![doc = include_str!("../README.md")]
use proc_macro::TokenStream;
use syn:: { ExprPath, parse_macro_input };
use quote::quote;
use const_frac::frac::syn::TokenFrac;

#[proc_macro]
pub fn frac(input: TokenStream) -> TokenStream {
    let result = parse_macro_input!(input as TokenFrac<ExprPath>);

    quote! { #result }.into()
}
