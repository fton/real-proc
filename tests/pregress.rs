#[test]
fn tests() {
    let t = trybuild::TestCases::new();
    t.pass("examples/frac.rs");
    t.pass("examples/simple.rs");
    //t.compile_fail("tests/04-variants-with-data.rs");
    //t.compile_fail("tests/05-match-expr.rs");
    //t.compile_fail("tests/06-pattern-path.rs");
    //t.compile_fail("tests/07-unrecognized-pattern.rs");
}
